import React, { useState } from 'react';
import ReactCardFlip from 'react-card-flip';
import Mariela from "../assets/Mariela.png"
import Danaela from "../assets/Danaela.png"
import Kristiyan from "../assets/Kristiyan.png"
import styled from 'styled-components';


const AboutUs = () => {
  const [isFlipped, setIsFlipped] = useState([false, false, false]);

  const handleClick = (index) => {
    const newFlippedState = [...isFlipped];
    newFlippedState[index] = !newFlippedState[index];
    setIsFlipped(newFlippedState);
  };

  const teamMembers = [
    {
      image: Mariela,
      name: 'Mariela Ivanova',
      info: 'Front-end Developer',
      links: 'https://www.linkedin.com/in/mariela-ivanova-1270771a8/',
    },
    {
      image: Kristiyan,
      name: 'Kristiyan Todorov',
      info: 'Front-end Developer',
      links: 'https://gitlab.com/kristiyanT',
    },
    {
      image: Danaela,
      name: 'Danaela Zlateva',
      info: 'Front-end Developer',
      links: 'https://www.linkedin.com/in/danaela-zlateva-7064461b5/?originalSubdomain=bg',
    }
  ];


  return (
    <div>
    <IntroTextContainer>
      <h1>Welcome to the heart and soul of our travelers' forum, where passion for exploration meets digital innovation!</h1>
      <p>We are Kristiyan, Mariela, and Danaela – a trio of travel enthusiasts with a shared vision: to create a space where fellow wanderers can come together, share their journeys, and find inspiration for their next adventure.</p>
    </IntroTextContainer>

    <AboutUsContainer>
      {teamMembers.map((member, index) => (
        <ReactCardFlip isFlipped={isFlipped[index]} flipDirection="horizontal" key={index}>
          <Card onClick={() => handleClick(index)}>
            <img src={member.image} alt={member.name} />
          </Card>
          <Card onClick={() => handleClick(index)}>
            <h2>{member.name}</h2>
            <p>{member.info}</p>
            <a href={member.links} target="_blank" rel="noopener noreferrer">LinkedIn</a>
          </Card>
        </ReactCardFlip>
      ))}
    </AboutUsContainer>
  </div>
  );
};

export default AboutUs;

const IntroTextContainer = styled.div`
  background-color: #f5f5f5;  
  padding: 2rem;
  margin-bottom: 2rem;
  border-radius: 10px; 
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);  
  max-width: 800px;  
  margin: 2rem auto;  

  h1 {
    font-size: 1.5rem;
    font-weight: bold;
    margin-bottom: 1rem;
  }

  p {
    font-size: 1.2rem;
    margin-bottom: 1rem;
  }
`;

const AboutUsContainer = styled.div`
  display: flex;
  justify-content: centered;
  gap: 2rem;
  padding: 2rem;
  padding-left: 600px
`;

const Card = styled.div`
  width: 200px;
  height: 300px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #f5f5f5;
  border-radius: 10px;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1); 
  cursor: pointer;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  h2 {
    font-size: 24px;
    margin: 1rem 0;
  }

  p {
    font-size: 18px;
    margin: 1rem 0;
  }

  a {
    color: blue;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }
`;
