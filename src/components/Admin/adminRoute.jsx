import { useEffect } from "react";
import { Outlet } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../provider/AuthProvider";

function AdminRoute() {
  const { currentUser } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (!currentUser || !(currentUser.email === "mariela.spsnv@gmail.com" || currentUser.email === "danaella.zlateva@gmail.com" || currentUser.email === "kristiyan.todorov@gmail.com")) {
      navigate("/");
    }

  }, [currentUser, navigate]);

  return <Outlet />;
}

export default AdminRoute;