import { useState, useEffect } from "react";
import { Container, Form, FormControl, Row, Col } from "react-bootstrap";
import { getAllComments } from "../../services/comments";
import { getAllPosts } from "../../services/post";
import { getAllUsers } from "../../services/users.services";
import {
  handleDeleteUser,
  handleBlockUser,
  handleDeletePost,
  handleDeleteComment,
} from "../../services/adminOperations";
import UserTable from "../Tables/UserTable";
import PostTable from "../Tables/PostTable";
import CommentTable from "../Tables/CommentTable";
import { styled } from "styled-components";

const AdminDashboard = () => {
  const [users, setUsers] = useState([]);
  const [posts, setPosts] = useState([]);
  const [filteredPosts, setFilteredPosts] = useState(posts);
  const [comments, setComments] = useState([]);
  const [postSearch, setPostSearch] = useState("");
  const [search, setSearch] = useState("");

  const [sortOrder, setSortOrder] = useState("desc");

  useEffect(() => {
    getAllUsers().then(setUsers);
    getAllPosts().then(setPosts);
  }, [])

  useEffect(() => {
    setFilteredPosts(
      posts.filter((post) =>
        post.title
          ? post.title.toLowerCase().includes(postSearch.toLowerCase())
          : false
      )
    );
  }, [postSearch, posts]);

  useEffect(() => {
    let allComments = [];

    posts.forEach((post) => {
      const handleCommentsUpdate = (commentsData) => {
        const commentsArray = commentsData ? Object.keys(commentsData).map(id => Object.assign({}, commentsData[id], { id, postId: post.id })) : [];
        allComments = [...allComments, ...commentsArray]
      };
      return getAllComments(post.id, handleCommentsUpdate);
    });

    setComments(allComments);
  }, [posts]);

  const filteredUsers =
    (Array.isArray(users) &&
      users.filter(
        (user) =>
          (user.email && user.email.includes(search)) ||
          (user.firstName && user.firstName.includes(search))
      )) ||
    [];

  const handleSortOrderChange = (e) => {
    setSortOrder(e.target.value);
    setFilteredPosts(
      filteredPosts.slice().sort((a, b) => {
        if (sortOrder === "desc") {
          return a.title.localeCompare(b.title);
        } else {
          return b.title.localeCompare(a.title);
        }
      })
    );
  };
  
  const deleteComment = (commentId, postId) => {
    handleDeleteComment(commentId, postId);
    setComments(comments.filter(c => c.commentId != commentId));
  }

  return (
    <Container>
      <Row>
        <Col md={10}>
          <h1>Admin Dashboard</h1>

          <h2>Users</h2>
          <Form>
            <FormControl
              type="text"
              placeholder="Search by email or name"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            />
          </Form>
          <UserTable
            users={filteredUsers}
            handleBlockUser={(userId, isBlocked) =>
              handleBlockUser(userId, isBlocked, setUsers)
            }
            handleDeleteUser={(userId) => handleDeleteUser(userId, setUsers)}
          />

          <h2>Posts</h2>
          <StyledForm>
            <FormControl
              type="text"
              placeholder="Search by title"
              value={postSearch}
              onChange={(e) => setPostSearch(e.target.value)}
            />
            <Form.Label>Sort by:</Form.Label>
            <Form.Control
              as="select"
              value={sortOrder}
              onChange={handleSortOrderChange}
            >
              <option value="desc">Descending</option>
              <option value="asc">Ascending</option>
            </Form.Control>
          </StyledForm>
          <PostTable
            posts={filteredPosts}
            users={users}
            handleDeletePost={(postId) => handleDeletePost(postId, setPosts)}
          />
          <h2>Comments</h2>
          <CommentTable
            comments={comments}
            users={users}
            posts={posts}
            handleDeleteComment={deleteComment}
          />
          <div>xx</div>
        </Col>
      </Row>
    </Container>
  );
};

export default AdminDashboard;

const StyledForm = styled(Form)`
    padding-bottom: 17px;
`;


