/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { styled } from "styled-components";
import background05 from "../assets/background05.jpg";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { getPosts } from "../services/post";
import { getAllUsers } from "../services/users.services";
import Posts from "../components/Posts/Posts";
import SiteInfo from "./SiteInfo";

function Main() {
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);
  const [sortedPosts, setSortedPosts] = useState([]);
  const [order, setOrder] = useState("Comments");

  useEffect(() => {
    getPosts((data) => {
      setPosts(data);
    });
    getAllUsers().then(setUsers);
  }, []);

  useEffect(() => {
   
  }, []);

  useEffect(() => {
    const data = [...posts];

    switch (order) {
      case 'Comments': {
        data.sort((a, b) => Object.values(b.comments).length - Object.values(a.comments).length);
        break;
      }
      case 'Ascending': {
        data.sort((a, b) => (a.title > b.title ? 1 : -1));
        break;
      }
      case 'Descending': {
        data.sort((a, b) => (a.title < b.title ? 1 : -1));
        break;
      }
      case 'MostLiked': {
        data.sort((a, b) => {
          console.log(`Comparing ${a.likes} and ${b.likes}`);
          b.likes - a.likes;
        })
        
        break;
      }
      case 'Recent': {
        data.sort((a, b) => b.createdOn - a.createdOn);
        break;
      }
      default:
        break;
    }

    setSortedPosts(data);
  }, [order, posts]);

  return (
    <BackgroundImage>
      <BtnContainer>
        <DropdownButton className="mb-2" title="Sort" onSelect={setOrder} selected={order}>
          <Dropdown.Item eventKey={"Comments"}>Most Commented</Dropdown.Item>
          <Dropdown.Item eventKey={"Ascending"}>Ascending</Dropdown.Item>
          <Dropdown.Item eventKey={"Descending"}>Descending</Dropdown.Item>
          <Dropdown.Item eventKey={"MostLiked"}>Most Liked</Dropdown.Item>
          <Dropdown.Item eventKey={"Recent"}>Recents</Dropdown.Item>
        </DropdownButton>
      </BtnContainer>
      <FlexBox>
        <PostContainer>
          <Posts posts={sortedPosts} />
        </PostContainer>
        <SiteInfoBox>
          <SiteInfo posts={posts} users={users}/>
        </SiteInfoBox>
      </FlexBox>
    </BackgroundImage>
  );
}

export default Main;

const BackgroundImage = styled.div`
  background-image: url(${background05});
  background-size: cover;
  min-height: 100vh; 
  width: 100%;
  
`;

const PostContainer = styled.div``;

const BtnContainer = styled.div`
  padding-top: 10px;
`;

const FlexBox = styled.div`
  display: flex;
  justify-content: center;
`;

const SiteInfoBox = styled.div`
  width: 20%;
`;