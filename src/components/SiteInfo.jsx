import { array, object } from "prop-types";
import styled from "styled-components";

/**
 * A component for displaying a site information - total posts / total users.
 * @component
 * @param {Object} props - The component props.
 * @param {Array} props.posts - The posts data array.
 * @param {Array} props.userData - The users data array.
 * @returns {JSX.Element} The rendered component.
 */

const SiteInfo = ({ posts, users }) => {
  return (
    <InfoWrapper className="web-app-info">
      <HeaderText className="web-app-header">Tourist talks</HeaderText>
      <SubHeaderText>Since 2023</SubHeaderText>
      <InfoText className="web-app-info-description">
        Tourist Talks is a vibrant and informative online platform dedicated to
        travel enthusiasts, wanderers, and globetrotters. As a comprehensive
        travel community, Tourist Talks offers travelers a space to share their
        experiences, exchange tips, and engage in discussions about various
        destinations, travel planning, cultural insights, and adventure stories.
      </InfoText>
      <StatsWrapper className="posts-users-info">
        <StatItem className="posts-info">
          <StatValue>{posts.length}</StatValue>
          <StatLabel>posts</StatLabel>
        </StatItem>
        <Divider>|</Divider>
        <StatItem className="users-info">
          <StatValue>{users.length}</StatValue>
          <StatLabel>members</StatLabel>
        </StatItem>
      </StatsWrapper>
    </InfoWrapper>
  );
};

SiteInfo.propTypes = {
  posts: array,
  users: array,
};

const InfoWrapper = styled.div`
  margin-top: 8px;
  background-color: rgba(211, 211, 211, 0.8);
  border: 1px solid black;
  padding: 20px;
  border-radius: 5px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
`;

const HeaderText = styled.h2`
  margin-bottom: 5px;
`;

const SubHeaderText = styled.p`
  color: #666;
  margin-bottom: 15px;
`;

const InfoText = styled.p`
  text-align: justify;
  margin-bottom: 10px;
`;

const StatsWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const StatItem = styled.div`
  display: flex;
  align-items: center;
  margin-right: 20px;
`;

const StatValue = styled.span`
  font-size: 24px;
  font-weight: bold;
  margin-right: 5px;
`;

const StatLabel = styled.span`
  color: #666;
`;

const Divider = styled.span`
  margin: 0 10px;
  color: #999;
`;

export default SiteInfo;
