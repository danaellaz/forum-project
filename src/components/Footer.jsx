import { Link } from "react-router-dom";
import { styled } from "styled-components";
import AboutUs from "./AboutUs";

/**
 * A component for displaying the footer.
 * @component
 * @returns {JSX.Element} The rendered component.
 */

export default function Footer() {
  return (
    <footer>
      <p>
        Team 13 @ 2023. All rights reserved.
        <StyledLink  to="/about-us">About Us</StyledLink >
        <GitLabSpan>
          <Link to="https://gitlab.com/team-0131/forum-project"> GitLab</Link>
        </GitLabSpan>
      </p>
    </footer>
  );
}

const GitLabSpan = styled.span`
  text-decoration: underline;
`;

const StyledLink = styled(Link)`
  text-decoration: underline;
`;
