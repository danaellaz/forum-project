import { Button } from "react-bootstrap";
import { deletePost } from "../services/post";
import { object } from "prop-types";
import { RiDeleteBin6Fill } from "react-icons/ri";

export const DeletePost = ({ post }) => {
  const handleDeleteBtn = () => {
    deletePost(post.id);
    window.location.reload();
  };

  return (
    <Button
      onClick={() => handleDeleteBtn()}
      variant="danger"
      className="position-absolute bottom-0 end-0 m-2"
    >
      <RiDeleteBin6Fill />
    </Button>
  );
};

DeletePost.propTypes = {
  post: object,
};
