import { useAuth } from "../../provider/AuthProvider";
import { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { getPostsByAuthor } from "../../services/post";
import { UserProfilePosts } from "./UserProfilePosts";
import { Link } from "react-router-dom";
import Badge from "react-bootstrap/Badge";
import Stack from "react-bootstrap/Stack";
import { UpdateUserInfo } from "./UpdateUserInfo";
import { updatePassword } from "firebase/auth";
import styled from "styled-components";

/**
 * A component for displaying the user's profile.
 * @component
 * @returns {JSX.Element} The rendered component.
 */

export const UserProfile = () => {
  const { user } = useAuth();
  const [userPosts, setUserPosts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { currentUser, userData } = useAuth();

  useEffect(() => {
    getPostsByAuthor(userData && userData.username).then((result) =>
      setUserPosts(result)
    );
  }, [userData]);

  const handleModal = () => setShowModal(!showModal);

  return (
    <StyledContainer>
      {userData && (
        <Row>
          <Col xs={9}>
            <h4 className="user-posts-header">User Posts</h4>
            {userPosts.length === 0 ? (
              <Link to="/posts/create" className="nav-link">
                <Button variant="btn btn-outline-success">Create Post</Button>
              </Link>
            ) : (
              userPosts.map((post) => (
                <div
                  key={post.id}
                  className={`mb-3 ${
                    post.id !== userPosts[userPosts.length - 1].id
                      ? "pb-3 border-bottom"
                      : ""
                  }`}
                >
                  <UserProfilePosts post={post} userData={userData} />
                </div>
              ))
            )}
          </Col>
          <Col className="user-info-chart">
            <StyledCard>
              <Card.Img
                variant="top"
                src={currentUser.photoURL || userData.image}
                className="mx-auto mt-3"
                style={{ width: "150px" }}
              />
              <Card.Body>
                {userData.isAdmin ? (
                  <Stack
                    direction="horizontal"
                    className="justify-content-center"
                  >
                    <Badge bg="success">Admin</Badge>
                  </Stack>
                ) : (
                  <Stack
                    direction="horizontal"
                    className="justify-content-center"
                  >
                    <Badge bg="secondary">User</Badge>
                  </Stack>
                )}
                <Card.Title>
                  {userData.firstName}
                  <Card.Subtitle className="mb-2 text-muted">
                    {userData.lastName}
                  </Card.Subtitle>
                </Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                  #{userData.username}
                </Card.Subtitle>

                <Card.Text style={{ textAlign: "center" }}>
                  {userData.email}
                </Card.Text>
                <UpdateUserInfo
                  variant="btn btn-outline-success"
                  user={userData}
                  show={showModal}
                  handleModal={handleModal}
                  currentUser={user}
                  updatePassword={updatePassword}
                />
              </Card.Body>
            </StyledCard>
          </Col>
        </Row>
      )}
    </StyledContainer>
  );
};

const StyledContainer = styled(Container)`
  padding-top: 20px;
`;

const StyledCard = styled(Card)`
  width: 18rem;

  .card-img-top {
    width: 150px;
    margin: 0 auto;
    margin-top: 0.75rem;
  }

  .user-info-chart {
    .card-title {
      margin-top: 0.5rem;
    }
  }
`;
