import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import { Fragment, useEffect, useState } from "react";
import { getAuth } from "firebase/auth";
import styled from "styled-components";
import { bool, func, object } from "prop-types";
import { uploadUserPhoto } from "../../services/users.services";
import { UserPicture } from "./UserPicture";
import { useAuth } from "../../provider/AuthProvider";

/**
 * A component for updating user information, including password.
 * @component
 * @param {Object} props - The component props.
 * @param {boolean} props.show - Indicates whether the modal is visible.
 * @param {Function} props.handleModal - Callback to handle modal close.
 * @param {Function} props.handleShow - Callback to handle modal show.
 * @param {Object} props.user - The user object containing user information.
 * @param {Function} props.updatePassword - Function to update the user's password.
 * @returns {JSX.Element} The rendered component.
 */

export const UpdateUserInfo = ({ show, handleModal, user, updatePassword }) => {
  const auth = getAuth();
  const curUser = auth.currentUser;

  const [newPassword, setNewPassword] = useState("");
  const [photo, setPhoto] = useState(null);
  const [photoURL, setPhotoURL] = useState(user.image);
  const [loading, setLoading] = useState(false);

  const handlePasswordChange = (event) => {
    setNewPassword(event.target.value);
  };

  const handlePhotoChange = (event) => {
    if (event.target.files[0]) {
      setPhoto(event.target.files[0]);
    }
  };

  useEffect(() => {
    if (curUser?.photoURL) {
      setPhotoURL(photoURL);
    }
  }, [photoURL]);

  const updateUserInformation = async () => {
    try {
      await updatePassword(curUser, newPassword);

      if (photo) {
        await uploadUserPhoto(photo, curUser, setLoading);
      }
      handleModal();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Fragment>
      <Button variant="btn btn-outline-success" onClick={handleModal}>
        Edit Profile
      </Button>
      <Modal
        show={show}
        onHide={handleModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          {!user.username ? (
            <ModalTitle>{user.firstName}</ModalTitle>
          ) : (
            <ModalTitle>#{user.username}</ModalTitle>
          )}
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formPlaintextEmail"
            >
              <Form.Label column sm="2">
                Email
              </Form.Label>
              <Col sm="10">
                <Form.Control plaintext readOnly defaultValue={user.email} />
              </Col>
            </Form.Group>
            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formPlaintextUsername"
            >
              <Form.Label column sm="2">
                Username
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  plaintext
                  readOnly
                  defaultValue={`#${user.username}`}
                />
              </Col>
            </Form.Group>
            <Form.Group
              as={Row}
              className="mb-3"
              sm=""
              controlId="formPlaintextPassword"
            >
              <Form.Label column sm="2">
                New Password
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={newPassword}
                  onChange={handlePasswordChange}
                />
              </Col>
            </Form.Group>
            <Form.Group controlId="formFile" className="mb-3">
              <Form.Label>Update photo</Form.Label>
              <Form.Control type="file" onChange={handlePhotoChange} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleModal}>
            Close
          </Button>
          <StyledButton variant="primary" onClick={updateUserInformation}>
            Update
          </StyledButton>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

UpdateUserInfo.propTypes = {
  user: object,
  show: bool,
  handleModal: func,
  currentUser: object,
  updatePassword: func,
};

const StyledButton = styled(Button)`
  background-color: transparent;
  color: #28a745;
  border-color: #28a745;
`;

const ModalTitle = styled(Modal.Title)`
  font-weight: bold;
`;
