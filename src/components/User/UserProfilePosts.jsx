import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { DeletePost } from "../DeletePost";
import { object } from "prop-types";

/**
 * A component for displaying a user's post in the user profile.
 * @component
 * @param {Object} props - The component props.
 * @param {Object} props.post - The post object containing post details.
 * @param {Object} props.userData - The user data object.
 * @returns {JSX.Element} The rendered component.
 */

export const UserProfilePosts = ({ post, userData }) => {
  return (
    <Link to={`/posts/view/${post.id}`}>
      <Card key={post.id} className="position-relative">
        <CardOverlay>
          <span className="me-2">
            {userData.username || userData.firstName}
          </span>
          <span>{post.createdOn.toLocaleDateString()}</span>
        </CardOverlay>
        <Card.Body>
          <Card.Title className="text-start">{post.title}</Card.Title>
          <Card.Text className="mb-2 text-muted text-start ">
            {post.content}
          </Card.Text>
        </Card.Body>
        <DeletePost post={post} />
      </Card>
    </Link>
  );
};

UserProfilePosts.propTypes = {
  post: object,
  userData: object,
};

const CardOverlay = styled.div`
  position: absolute;
  top: 0;
  margin: 0.5rem;
  background-color: #f8f9fa;
  padding: 0.5rem;
  border-radius: 0.25rem;

  span {
    margin-right: 0.5rem;
  }
`;
