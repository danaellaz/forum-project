import { useAuth } from "../../provider/AuthProvider";
import styled from "styled-components";

/**
 * A component for displaying the user's profile picture.
 * @component
 * @returns {JSX.Element} The rendered component.
 */

export const UserPicture = () => {
  const { userData } = useAuth();

  if (!userData) {
    return <div>{null}</div>;
  }

  return <StyledImage src={userData.image} />
};

const StyledImage = styled.img`
  width: 50px;
  border-radius: 50%
`;
