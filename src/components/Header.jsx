import { useRef } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../provider/AuthProvider";
import { Button, Container, Form, Nav, Navbar } from "react-bootstrap";
import { styled } from "styled-components";
import { UserPicture } from "./User/UserPicture";
import logo001 from "../assets/logo001.png";

export default function Header() {
  const termRef = useRef();
  const location = useLocation();
  const navigate = useNavigate();
  const user = useAuth();

  const handleSearch = (e) => {
    e.preventDefault();
    const term = termRef.current.value;
    termRef.current.value = "";
    navigate(`/search/${term}`);
  };

  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container fluid>
        <Link to={"/"}>
          <img
            src={logo001}
            alt="Tourist talks Logo"
            width="100"
            height="70"
            className="d-inline-block align-top"
          />
        </Link>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <div>
            <InnerWrapper className="me-auto my-2 my-lg-0" navbarScroll>
              <Link to="/" className="nav-link">
                Home
              </Link>
              <Link to="/topics" className="nav-link">
                Topics
              </Link>
              <Link to="/posts/create" className="nav-link">
                Create post
              </Link>
              {user.currentUser &&
              (user.currentUser.email === "mariela.spsnv@gmail.com" ||
                user.currentUser.email === "danaella.zlateva@gmail.com" ||
                user.currentUser.email === "kristiyan.todorov@gmail.com") ? (
                <Link to="/admin-dashboard" className="nav-link">
                  Admin Dashboard
                </Link>
              ) : (
                <>{null}</>
              )}
              <div>
                <Form className="d-flex" onSubmit={handleSearch}>
                  <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    ref={termRef}
                    required
                  />
                  <Button type="submit" variant="outline-success">
                    Search
                  </Button>
                </Form>
              </div>
            </InnerWrapper>
          </div>
        </Navbar.Collapse>
        <UserControls>
          {!["/signup", "/login"].includes(location.pathname) &&
            !user.isAuthenticated() && (
              <Link to="/login" className="auth-link">
                Login
              </Link>
            )}
          {user.isAuthenticated() && (
            <div>
              <Link to="user-info">
                <UserPicture className="nav-bar-user-picture" />
              </Link>
              (
              <Link to="/logout" className="auth-link">
                Logout
              </Link>
              )
            </div>
          )}
        </UserControls>
      </Container>
    </Navbar>
  );
}

const InnerWrapper = styled(Nav)`
  max-height: 100px;
  background-color: white @media (orientation: portrait) {
    max-height: 130px;
  }
`;

const UserControls = styled.div`
  padding-right: 2px;
`;
