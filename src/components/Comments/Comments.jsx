import { useState, useEffect } from "react";
import {
  Button,
  Col,
  Container,
  FloatingLabel,
  Form,
  Toast,
} from "react-bootstrap";
import { addComment, getAllComments } from "../../services/comments";
import { styled } from "styled-components";
import { useParams } from "react-router";
import { useAuth } from "../../provider/AuthProvider";
import { PostLikes } from "../Posts/PostLikes";
import SingleComment from "./SingleComment";
import { BiSolidComment } from "react-icons/bi";

const Comments = () => {
  const [comment, setComment] = useState("");
  const [allComments, setAllComments] = useState({});
  const { id } = useParams("");
  const { currentUser, userData } = useAuth();
  const [showA, setShowA] = useState(false);

  const toggleShowA = () => setShowA(!showA);

  useEffect(() => {
    getAllComments(id, setAllComments);
  }, [id]);

  const handleCommentChange = (event) => {
    setComment(event.target.value);
  };

  const sendComments = async () => {
    const newComment = {
      author: userData.username,
      date: Date.now(),
      content: comment,
      authorId: userData.uid,
    };
    await addComment(id, newComment);
    setComment("");
  };
  console.log(allComments)
  return (
    <Container>
      <Col>
        <PostLikes postId={id} currentUser={currentUser} />
      <Col md={6} className="mb-2">
        {!allComments ? (
          <p>Currently there are <strong>NO</strong> comments!</p>
        ) : (
        <Button onClick={toggleShowA} className="mb-2">
          Show <strong>all</strong> Comments
        </Button>
        )}
        <OptionsToast show={showA} onClose={toggleShowA}>
            {Object.entries(allComments || {}).map(([commentId, comment]) => (
              <SingleComment
                key={commentId}
                comment={comment}
                commentId={commentId}
                postId={id}
                isAuthor={userData.username === comment.author}
              />
            ))}
          </OptionsToast>
        </Col>
        <Col>
          <FloatingLabel
            controlId="floatingTextarea"
            label={!comment ? "Write a comment here" : ""}
          >
            <Form.Control
              as="textarea"
              placeholder={"Leave a comment here"}
              value={comment}
              onChange={handleCommentChange}
            ></Form.Control>
          </FloatingLabel>
        </Col>
      </Col>
      <OptionsButton
        variant="outline-primary"
        disabled={!comment}
        onClick={sendComments}
      >
        <BiSolidComment />
      </OptionsButton>
    </Container>
  );
};
export default Comments;

const OptionsButton = styled(Button)`
  margin: 10px;
`;

const OptionsToast = styled(Toast)`
  width: 80%;
  max-width: 500px;
`;
