import { bool, object, string } from "prop-types";
import { Button, Card } from "react-bootstrap";
import { deleteComment } from "../../services/comments";
import { styled } from "styled-components";
import { Fragment, useEffect, useState } from "react";
import { getUserByHandle } from "../../services/users.services";
import { RiDeleteBin6Fill } from "react-icons/ri";

const SingleComment = ({ comment, commentId, postId, isAuthor }) => {
  const [src, setSrc] = useState();

  const handleDeleteComment = () => {
    deleteComment(postId, commentId);
  };

  useEffect(() => {
    getUserByHandle(comment.authorId).then((result) => setSrc(result.val()));
  }, [comment.authorId]);

  return (
    <Fragment>
      <Wrapper>
        <WrapperContent>
          <CommentContent>
            <UserImageContainer>
              <UserImage
                src={src ? src.image : ""}
                className="nav-bar-user-picture"
              />
            </UserImageContainer>
            <CardWrapper>
              <h6>{comment.author}</h6>
              <p className="small">{new Date(comment.date).toLocaleString()}</p>
              <p>{comment.content}</p>
              {isAuthor && (
                <Button
                  onClick={handleDeleteComment}
                  variant="danger"
                  className="position-absolute bottom-0 end-0 m-2"
                >
                  <RiDeleteBin6Fill />
                </Button>
              )}
            </CardWrapper>
          </CommentContent>
        </WrapperContent>
      </Wrapper>
    </Fragment>
  );
};

export default SingleComment;

SingleComment.propTypes = {
  comment: object,
  commentId: string,
  postId: string,
  username: string,
  isAuthor: bool,
};

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const WrapperContent = styled.div`
  width: 80%;
  max-width: 500px;
`;

const CommentContent = styled.div`
  display: flex;
  justify-content: flex-start;
  margin-bottom: 2rem;
`;

const CardWrapper = styled(Card)`
  width: 80%;
`;

const UserImageContainer = styled.div`
  width: 50px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
`;

const UserImage = styled.img`
  max-width: 100%;
  max-height: 100%;
  border-radius: 50%;
`;
