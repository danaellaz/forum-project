import { BsTrash3 } from 'react-icons/bs'
import { Table, Button } from 'react-bootstrap';
import { styled } from "styled-components";

const CommentTable = ({ comments, users, posts, handleDeleteComment }) => {

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Comment Text</th>
          <th>Post Title</th>
          <th>User Email</th>
          <th>Date</th>
          <StyledTh>Delete</StyledTh>
        </tr>
      </thead>
      <tbody>
        {comments.map((comment) => (
          <tr key={comment.id}>
            <td>{comment.content}</td>
            <td>{posts.find((post) => post.id === comment.postId)?.title || 'Unknown'}</td>
            <td>{users.find((user) => user.id === comment.authorId)?.email || 'Unknown'}</td>
            <td>{new Date(comment.date).toLocaleString()}</td>
            <td>
              <Button onClick={() => handleDeleteComment(comment.id, comment.postId)} >
                <BsTrash3 />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default CommentTable;

const StyledTh = styled.th`
  width: 117px;
`;
