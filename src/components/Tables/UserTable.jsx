import React from "react";
import { Table, Button } from "react-bootstrap";
import { FaBan, FaCheck} from "react-icons/fa";
import { BsTrash3 } from 'react-icons/bs'

const UserTable = ({ users, handleBlockUser, handleDeleteUser }) => {
  
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>First name</th>
          <th>Email</th>
          <th>Registration Date</th>
          <th>Block</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {users.map((user) => (
          <tr key={user.id}>
            <td>{user.firstName}</td>
            <td>{user.email}</td>
            <td>{user.registrationDate ? user.registrationDate : 'No date'}</td>
            <td>
              <Button
                variant={user.isBlocked ? "success" : "danger"}
                onClick={() => handleBlockUser(user.id, user.isBlocked)}
              >
                {user.isBlocked ? <FaCheck /> : <FaBan />}
              </Button>
            </td>
            <td>
              <Button
                onClick={() => handleDeleteUser(user.id)}
              >
                <BsTrash3 />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default UserTable;
