import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { BsTrash3 } from 'react-icons/bs'
import { useNavigate } from 'react-router-dom';

const PostTable = ({ posts, users, handleDeletePost }) => {
  const navigate = useNavigate();

  const handlePostClick = (postId) => {
    navigate(`/posts/${postId}`);
  };

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Title</th>
          <th>Author Email</th>
          <th>Creation Date</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {posts.map((post) => (
          <tr key={post.id} onClick={() => handlePostClick(post.id)}>
            <td>{post.title}</td>
            <td>{users.find((user) => user.username === post.author)?.email || 'Unknown'}</td>
            <td>{new Date(post.createdOn).toLocaleString()}</td>
            <td>
              <Button onClick={(e) => { e.stopPropagation(); handleDeletePost(post.id); }}>
                <BsTrash3 />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default PostTable;
