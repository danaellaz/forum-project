import { Navigate, Outlet } from "react-router-dom";
import { useAuth } from "../../provider/AuthProvider";

function PrivateRoute() {
  const { currentUser, isBlocked } = useAuth();

  if (!currentUser || isBlocked) {
    return <Navigate to="/login" />;
  }

  return <Outlet />;
}

export default PrivateRoute;
