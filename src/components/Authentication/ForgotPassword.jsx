import { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../../provider/AuthProvider";
import { Link } from "react-router-dom";
import { styled } from "styled-components";

export default function ForgotPassword() {
  const email = useRef();
  const { resetPassword } = useAuth();
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      setMessage("");
      setError("");
      setLoading(true);
      await resetPassword(email.current.value);
      setMessage("Check your inbox for instructions");
      email.current.value = "";
    } catch (e) {
      setError(e.message !== "" ? e.message : `Failed to reset password`);
    }

    setLoading(false);
  }

  return (
    <Wrapper>
      <InnerWrapper>
        <div className="text-center mt-5">
          <Card>
            <Card.Body>
              <h2 className="text-center mb-4">Password Reset</h2>
              {error && <Alert variant="danger">{error}</Alert>}
              {message && <Alert variant="success">{message}</Alert>}
              <Form onSubmit={handleSubmit}>
                <Form.Group id="email">
                  <Form.Label className="mt-2">Email</Form.Label>
                  <Form.Control type="email" ref={email} required />
                </Form.Group>
                <Button disabled={loading} className="w-100 mt-4" type="submit">
                  Reset Password
                </Button>
              </Form>
              <div className="w-100 text-center mt-3">
                <Link to="/login">Login</Link>
              </div>
            </Card.Body>
          </Card>
          <div className="w-100 text-center mt-2 pb-5">
            Need an account? <Link to="/signup">Sign Up</Link>
          </div>
        </div>
      </InnerWrapper>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const InnerWrapper = styled.div`
  width: 100%;
  max-width: 400px;
`;
