import { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../../provider/AuthProvider";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";

export default function Signup() {
  const email = useRef();
  const firstName = useRef();
  const lastName = useRef();
  const password = useRef();
  const passwordConfirm = useRef();
  const username = useRef();
  const navigate = useNavigate();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const { signup } = useAuth();

  async function handleSubmit(e) {
    e.preventDefault();

    if (password.current.value !== passwordConfirm.current.value) {
      return setError("Passwords do not match");
    }

    if (firstName.length < 4 || firstName.length > 32) {
      return setError("First name must be between 4 and 32 characters");
    }

    if (lastName.length < 4 || lastName.length > 32) {
      return setError("Last name must be between 4 and 32 characters");
    }

    if (
      username.current.value.length < 4 ||
      username.current.value.length > 8
    ) {
      return setError("Username must be between 4 and 8 characters");
    }

    try {
      setError("");
      setLoading(true);
      await signup(
        email.current.value,
        password.current.value,
        firstName.current.value,
        lastName.current.value,
        username.current.value
      );
      navigate("/");
    } catch (e) {
      setError("Failed to create an account");
    }

    setLoading(false);
  }

  return (
    <Wrapper>
      <InnerWrapper>
        <div className="text-center sign-up-wrapper">
          <Card>
            <Card.Body>
              <h2 className="text-center mb-4">Sign Up</h2>
              {error && <Alert variant="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit}>
                <Form.Group id="firstName">
                  <Form.Label className="mt-2">First Name</Form.Label>
                  <Form.Control type="text" ref={firstName} required />
                </Form.Group>
                <Form.Group id="lastName">
                  <Form.Label className="mt-2">Last Name</Form.Label>
                  <Form.Control type="text" ref={lastName} required />
                </Form.Group>
                <Form.Group id="email">
                  <Form.Label className="mt-2">Email</Form.Label>
                  <Form.Control type="email" ref={email} required />
                </Form.Group>
                <Form.Group id="username">
                  <Form.Label className="mt-2">Username</Form.Label>
                  <Form.Control type="text" ref={username} required />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label className="mt-2">Password</Form.Label>
                  <Form.Control type="password" ref={password} required />
                </Form.Group>
                <Form.Group id="password-confirm">
                  <Form.Label className="mt-2">
                    Password Confirmation
                  </Form.Label>
                  <Form.Control
                    type="password"
                    ref={passwordConfirm}
                    required
                  />
                </Form.Group>
                <Button disabled={loading} className="w-100 mt-4" type="submit">
                  Sign Up
                </Button>
              </Form>
            </Card.Body>
          </Card>
          <div className="w-100 text-center mt-2 pb-5">
            Already have an account?{" "}
            <Link to="/login" className="auth-link">
              Log In
            </Link>
          </div>
        </div>
      </InnerWrapper>
    </Wrapper>
  );
}
const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 15px;
`;

const InnerWrapper = styled.div`
  width: 100%;
  max-width: 400px;
`;
