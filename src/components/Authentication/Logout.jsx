import { useEffect } from "react";
import { useAuth } from "../../provider/AuthProvider";
import { useNavigate } from "react-router-dom";

export default function Logout() {
  const { logout } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    logout();
    navigate("/");
  });

  return <div></div>;
}
