import { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../../provider/AuthProvider";
import { Link, useNavigate } from "react-router-dom";
import { styled } from "styled-components";

function Login() {
  const email = useRef();
  const password = useRef();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const { login } = useAuth();

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      setError("");
      setLoading(true);
      await login(email.current.value, password.current.value);
      navigate("/");
    } catch (e) {
      setError(e.message !== "" ? e.message : `Failed to log in`);
    }
    setLoading(false);
  }

  return (
    <Wrapper>
      <InnerWrapper>
        <div className="text-center mt-5">
          <Card>
            <Card.Body>
              <h2 className="text-center mb-4">Log In</h2>
              {error && <Alert variant="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit}>
                <Form.Group id="email">
                  <Form.Label className="mt-2">Email</Form.Label>
                  <Form.Control type="email" ref={email} required />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label className="mt-2">Password</Form.Label>
                  <Form.Control type="password" ref={password} required />
                </Form.Group>
                <Button disabled={loading} className="w-100 mt-4" type="submit">
                  Log In
                </Button>
              </Form>
              <div className="w-100 text-center mt-3">
                <Link to="/forgot-password" className="auth-link">
                  Forgot Password?
                </Link>
              </div>
            </Card.Body>
          </Card>
          <div className="w-100 text-center mt-2 pb-5">
            Need an account?{" "}
            <Link to="/signup" className="auth-link">
              Sign Up
            </Link>
          </div>
        </div>
      </InnerWrapper>
    </Wrapper>
  );
}

export default Login;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const InnerWrapper = styled.div`
  width: 100%;
  max-width: 400px;
`;
