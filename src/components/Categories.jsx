import React, { useEffect, useState } from 'react';
import { Container, ButtonGroup, Button, Row } from 'react-bootstrap';
import styled from 'styled-components';
import { getPostsByCategory } from '../services/post';
import SinglePost from './Posts/SinglePost';
import background05 from "../assets/background05.jpg";

const CATEGORIES = ['Skiing', 'Hiking', 'Spa', 'Cycling'];

const Categories = ({ onCategoryChange }) => {
  
  const [selectedCategory, setSelectedCategory] = useState('Hiking'); 
  const [allPosts, setAllPosts] = useState([])

  const handleCategoryClick = (category) => {
    setSelectedCategory(category);
    if(onCategoryChange) {
      onCategoryChange(category);
    }
  };

  useEffect(() => {
    const fetchData = async() => {
      try{
        const allPostByCategory = await getPostsByCategory(selectedCategory.toLocaleLowerCase());
        setAllPosts(allPostByCategory)
      } catch (e) {
        console.error(`Error while getting all posts by category: ${e}`)
      }
    }
    fetchData();
  }, [selectedCategory])
  
  console.log(selectedCategory)
  console.log(allPosts)

  return (
    <BackgroundImage>
      <PostWrapper className="forum-view">
        <CategoriesWrapper>
          <ButtonGroup>
            {CATEGORIES.map((category) => (
              <CategoryButton 
                key={category}
                variant={selectedCategory === category ? 'primary' : 'secondary'}
                onClick={() => handleCategoryClick(category)}
              >
                {category}
              </CategoryButton>
            ))}
          </ButtonGroup>
        </CategoriesWrapper>
        <PostRow>
              {allPosts.map((post) => (
                <SinglePost key={post.id} post={post} />
              ))}
        </PostRow>
      </PostWrapper>
    </BackgroundImage>
  );
};

export default Categories;

const PostWrapper = styled(Container)`
  margin-top: 8px;
  padding: 20px;
`;

const CategoriesWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 20px 0;
`;

const CategoryButton = styled(Button)`
  margin: 0 5px;
`;

const PostRow = styled(Row)`
  justify-content: space-around;
`;

const BackgroundImage = styled.div`
  background-image: url(${background05});
  background-size: cover;
  min-height: 100vh; 
  width: 100%;
`;