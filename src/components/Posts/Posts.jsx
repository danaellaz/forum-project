/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { Container, Row } from "react-bootstrap";
import SinglePost from "./SinglePost";
import styled from "styled-components";
import { array } from "prop-types";
import { getImage } from "../../services/post";

export const Posts = ( { posts } ) => {
  const [postsId, setPosts] = useState([]);

  useEffect(() => {
    Promise.all(
      posts.map(async (post) => {
        const imageUrl = await getImage(post.id);

        return {
          ...post,
          imageUrl: imageUrl,
        };
      })
    ).then((postsWithImageUrls) => setPosts(postsWithImageUrls));
  }, [posts]);


  return (
    <PostWrapper className="forum-view">
      <PostRow>
        {postsId.map((post) => (
          <SinglePost key={post.id} post={post} />
        ))}
      </PostRow>
    </PostWrapper>
  );

};

export default Posts;

Posts.propTypes = {
  posts: array,
};


const PostWrapper = styled(Container)`
  margin-top: 8px;
`;

const PostRow = styled(Row)`
  justify-content: space-around;
`;
