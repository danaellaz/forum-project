import { object } from "prop-types";
import { Card, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { styled } from "styled-components";

const SinglePost = ({ post }) => {
  return (
    <Col md={4} key={post.id} className="mb-4">
      <CardWrapper>
        <Card.Body className="d-flex flex-column">
          <Card.Title>
            <Link to={`/posts/view/${post.id}`}>{post.title}</Link>
          </Card.Title>
          <Card.Text className="text-muted">
            {new Date(post.createdOn).toLocaleDateString()}
          </Card.Text>
          <Card.Text>
            Category: {post.category}
          </Card.Text>
        </Card.Body>
      </CardWrapper>
    </Col>
  );
};

export default SinglePost;

SinglePost.propTypes = {
  post: object,
};

const CardWrapper = styled(Card)`
  height: 100%;
  border: 1px solid black;
  background-color: rgba(211, 211, 211, 0.8);
`;
