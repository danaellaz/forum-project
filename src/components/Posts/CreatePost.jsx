import { useState } from "react";
import { db } from "../../config/firebase-config";
import { ref, push, set, update } from "firebase/database";
import {
  getStorage,
  ref as storageRef,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { useAuth } from "../../provider/AuthProvider";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { styled } from "styled-components";
import { useNavigate } from "react-router-dom";

function CreatePost() {
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [image, setImage] = useState(null);
  const [comments, setComments] = useState([]);
  const [likes, setLikes] = useState({});
  const [uploading, setUploading] = useState(false);
  const { currentUser, userData } = useAuth();
  const [error, setError] = useState("");
  const [category, setCategory] = useState("");

  const handleCreatePost = (e) => {
    e.preventDefault();

    if (!currentUser) {
      setError("You must be logged in to create a post.");
      return;
    }

    if (title.length < 16 || title.length > 64) {
      setError("Title must be between 16 and 64 characters.");
      return;
    }

    if (content.length < 32 || content.length > 8192) {
      setError("Content must be between 32 and 8192 characters.");
      return;
    }

    const newPostRef = push(ref(db, "posts"));
    const author = userData.username;
    const createdOn = Date.now();

    const userPostsRef = ref(db, `users/${currentUser.uid}/createdPosts`);
    update(userPostsRef, { [newPostRef.key]: true });

    if (image) {
      const storage = getStorage();
      const storageRefs = storageRef(
        storage,
        `posts/${newPostRef.key}/${image.name}`
      );
      const uploadTask = uploadBytesResumable(storageRefs, image);

      setUploading(true);

      uploadTask.on(
        "state_changed",
        () => {},
        (error) => {
          console.log(error);
          setUploading(false);
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            set(newPostRef, {
              author,
              title,
              content,
              image: downloadURL,
              comments,
              likes,
              createdOn,
              category
            });
          });
          setUploading(false);
        }
      );
    } else {
      set(newPostRef, {
        author,
        title,
        content,
        comments,
        likes,
        createdOn,
        category
      });
    }

    setTitle("");
    setContent("");
    setImage(null);
    setComments([]);
    setLikes({});
    setCategory("");
    navigate("/");
  };

  return (
    <Wrapper>
      <InnerWrapper>
        <Card.Body>
          <h2>Create new Post</h2>
          {error && <Alert variant="danger">{error}</Alert>}
          <Form onSubmit={handleCreatePost}>
            <Form.Group>
              <Form.Label>Title:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Content:</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Enter content"
                value={content}
                onChange={(e) => setContent(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Category:</Form.Label>
              <Form.Select value={category} onChange={(e) => setCategory(e.target.value)}>
                <option value="" disabled>Select a category</option>
                <option value="hiking">Hiking</option>
                <option value="skiing">Skiing</option>
                <option value="spa">Spa</option>
                <option value="cycling">Cycling</option>
              </Form.Select>
            </Form.Group>
            <Form.Group>
              <Form.Label>Image:</Form.Label>
              <Form.Control
                type="file"
                onChange={(e) => setImage(e.target.files[0])}
              />
            </Form.Group>
            {uploading && <div>Uploading...</div>}
            <CreatePostButton variant="primary" type="submit">
              Create Post
            </CreatePostButton>
          </Form>
        </Card.Body>
      </InnerWrapper>
    </Wrapper>
  );
}

export default CreatePost;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 20px;
`;

const InnerWrapper = styled(Card)`
  min-width: 600px;
`;

const CreatePostButton = styled(Button)`
  margin-top: 15px;
`;
