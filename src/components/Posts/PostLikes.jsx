import { addLike, getAllLikes, removeLike } from "../../services/likes";
import { useState, useEffect } from "react";
import { Col, Button, Container, Card, Row } from "react-bootstrap";
import { styled } from "styled-components";
import { string, object } from "prop-types";
import { db } from "../../config/firebase-config";
import { update, ref, remove } from "firebase/database";
import { AiFillLike, AiFillDislike } from "react-icons/ai";
/**
 * A component for handling likes and dislikes for a post.
 * @component
 * @param {Object} props - The component props.
 * @param {string} props.postId - The ID of the post.
 * @param {Object} props.currentUser - The current user object.
 * @returns {JSX.Element} The rendered component.
 */

export const PostLikes = ({ postId, currentUser }) => {
  const [like, setLike] = useState(null);
  const [allLikes, setAllLikes] = useState([]);

  const sendLike = async () => {
    const newLike = { author: currentUser.uid, post: postId, date: Date.now() };
    setLike(newLike);

    const userLikeRef = ref(db, `users/${currentUser.uid}/likedPosts`);
    await update(userLikeRef, { [newLike.post]: postId });

    try {
      await addLike(postId, newLike);
      setAllLikes((prevLikes) => ({
        ...prevLikes,
        [postId]: newLike,
      }));
    } catch (e) {
      console.error(`Error adding new like: ${e}`);
    }
  };

  const fetchLikes = async () => {
    try {
      const likesData = await getAllLikes(postId);
      setAllLikes(likesData);
    } catch (e) {
      console.error(`Error while fetching likes: ${e}`);
    }
  };

  useEffect(() => {
    fetchLikes();
  }, []);

  const removeLiked = async () => {
    try {
      await removeLike(postId, like);
      setLike(null);
      const updatedLikes = await getAllLikes(postId);
      setAllLikes(updatedLikes);
      await remove(ref(db, `users/${currentUser.uid}/likedPosts/${postId}`));
    } catch (e) {
      console.error(`Error while removing like: ${e}`);
    }
  };

  return (
    <Container>
      <Row>
        <Col>
          {like && like.author === currentUser.uid ? (
            <OptionsButton variant="outline-primary" onClick={removeLiked}>
             <AiFillDislike/>
             <div className="like-count">
                <Card.Text>Likes: {Object.entries(allLikes).length}</Card.Text>
              </div>
            </OptionsButton>
          ) : (
            <OptionsButton variant="outline-primary" onClick={sendLike}>
              <AiFillLike />
              <div className="like-count">
                <Card.Text>Likes: {Object.entries(allLikes).length}</Card.Text>
              </div>
            </OptionsButton>
          )}
        </Col>
        <Col>
          
        </Col>
      </Row>
    </Container>
  );
};

PostLikes.propTypes = {
  postId: string,
  currentUser: object,
};

const OptionsButton = styled(Button)`
  margin: 10px;
`;
