export const SearchBar = () => {
  return (
    <form className="search-from">
      <input
        className="search-bar"
        id="search"
        type="search"
        placeholder="Search..."
      />
      <button id="search-icon">
        Search
      </button>
    </form>
  );
};
