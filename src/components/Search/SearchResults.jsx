import { useState, useEffect } from "react";
import { getImage } from "../../services/post";
import { Container, Row } from "react-bootstrap";
import Posts from "../Posts/Posts";
import { styled } from "styled-components";
import { getAllPosts } from "../../services/post";
import { useParams } from "react-router-dom";

export const SearchResults = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const { term } = useParams();

  useEffect(() => {
    setLoading(true);
    getAllPosts().then((allPosts) => {
      const filteredPosts = allPosts.filter((post) =>
        post.title.toLowerCase().includes(term.toLowerCase())
      );
      Promise.all(
        filteredPosts.map(async (post) => {
          const imageUrl = await getImage(post.id);
          return {
            ...post,
            imageUrl: imageUrl,
          };
        })
      )
        .then((postsWithImageUrls) => setPosts(postsWithImageUrls))
        .then(() => setLoading(false));
    });
  }, [term]);

  if (loading) {
    return (
      <Wrapper>
        <h3>Searching...</h3>
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      {term && posts.length > 0 ? (
        <h3>Results for: {term}</h3>
      ) : (
        term && <h3>There is no result for: {term}</h3>
      )}
      <RowWrapper>
        <Posts posts={posts} />
      </RowWrapper>
    </Wrapper>
  );
};

export default SearchResults;

const Wrapper = styled(Container)`
  margin-top: 8px;
  padding: 20px;
`;

const RowWrapper = styled(Row)`
  justify-content: space-around;
`;
