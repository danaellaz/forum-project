import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDIg0w6yuK-sPb1nwAJ5cMy-mo-cLvaiyo",
  authDomain: "forum-web-project.firebaseapp.com",
  projectId: "forum-web-project",
  storageBucket: "forum-web-project.appspot.com",
  messagingSenderId: "185731453282",
  appId: "1:185731453282:web:1d441fbeb52a85ce8b1c3b",
  databaseURL: "https://forum-web-project-default-rtdb.europe-west1.firebasedatabase.app/"
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
