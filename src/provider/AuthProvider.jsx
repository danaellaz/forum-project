import React, { useContext, useState, useEffect } from "react";
import { auth, db } from "../config/firebase-config";
import { ref, set, get } from "firebase/database";
import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
} from "firebase/auth";
import { getUserData } from "../services/users.services";

const AuthContext = React.createContext();

export function useAuth() {
  return useContext(AuthContext);
}

// eslint-disable-next-line react/prop-types
export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState();
  const [isBlocked, setBlocked] = useState(false);
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = useState(null);

  function signup(email, password, firstName, lastName, username) {
    return createUserWithEmailAndPassword(auth, email, password).then(
      ({ user }) => {
        const initials = `${firstName.charAt(0)}${lastName.charAt(0)}`;
        const image = `https://ui-avatars.com/api/?name=${initials}&background=random&rounded=true&size=128`;
        const createdPosts = [0];
        const likedPosts = [0];
        const isAdmin = false;
        const registrationDate = new Date().toLocaleDateString();

        const userRef = ref(db, `users/${user.uid}`);
        set(userRef, {
          firstName,
          lastName,
          email,
          image,
          createdPosts,
          likedPosts,
          isAdmin,
          isBlocked,
          registrationDate,
          username,
        });

        return user;
      }
    );
  }

  function login(email, password) {
    return signInWithEmailAndPassword(auth, email, password).then(
      async ({ user }) => {
        const userRef = ref(db, `users/${user.uid}`);
        const userSnapshot = await get(userRef);
        const userData = userSnapshot.val();

        if (userData.isBlocked) {
          setBlocked(true);
          logout();
          return;
        } else {
          setBlocked(false);
        }
      }
    );
  }

  function logout() {
    return auth.signOut();
  }

  function resetPassword(email) {
    return sendPasswordResetEmail(auth, email);
  }

  function updatePassword(password) {
    return currentUser.updatePassword(password);
  }

  function isAuthenticated() {
    return currentUser !== null;
  }

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(async (user) => {
      setCurrentUser(user);
      setLoading(false);
    });

    return unsubscribe;
  }, []);

  useEffect(() => {
    const userDataObj = async () => {
      if (currentUser) {
        const userDataSnapshot = await getUserData(currentUser.uid);
        setUserData({ ...userDataSnapshot, uid: currentUser.uid });
      } else {
        return null;
      }
    };
    userDataObj();
  }, [currentUser]);

  const value = {
    currentUser,
    userData,
    isBlocked,
    signup,
    login,
    logout,
    resetPassword,
    updatePassword,
    isAuthenticated,
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
}
