import {
  getAllUsers,
  deleteUser,
  blockUser,
  unblockUser,
} from "./users.services";
import { deletePost, getAllPosts } from "./post";
import { getAllComments, deleteComment } from "./comments";

export const handleDeleteUser = async (userId, setUsers) => {
  await deleteUser(userId);
  const updatedUsers = await getAllUsers();
  setUsers(updatedUsers);
};

export const handleBlockUser = async (userId, isBlocked, setUsers) => {
  if (isBlocked) {
    await unblockUser(userId);
  } else {
    await blockUser(userId);
  }
  const updatedUsers = await getAllUsers();
  setUsers(updatedUsers);
};

export const handleDeletePost = async (postId, setPosts) => {
  await deletePost(postId);
  const updatedPosts = await getAllPosts();
  setPosts(updatedPosts);
};

export async function handleDeleteComment(commentId, postId) {
  await deleteComment(postId, commentId);
}
