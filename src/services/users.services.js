import { 
  get, 
  set, 
  ref, 
  child, 
  remove, 
  update } from "firebase/database";
import { db } from "../config/firebase-config";
import { storage } from "../config/firebase-config";
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";
import { updateProfile } from "firebase/auth";

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (handle, uid, username) => {
  return set(ref(db, `users/${handle}`), {
    handle,
    uid,
    username,
    createdOn: Date.now(),
    likedPosts: {},
  });
};

export const getUserData = async (uid) => {
  try {
    const dbRef = ref(db);
    const snapshot = await get(child(dbRef, `users/${uid}`));
    if (snapshot.exists()) {
      const userDataSnapshot = snapshot.val();
      return userDataSnapshot;
    } else {
      console.log(`No data available`);
      return null;
    }
  } catch (error) {
    console.error(error);
    return null;
  }
};

export const getAllUsers = async () => {
  try {
    const snapshot = await get(ref(db, "users"));
    const usersData = snapshot.val();
    const usersArray = Object.entries(usersData || {}).map(([id, user]) => ({
      id,
      ...user,
      createdPosts: user.createdPosts ? user.createdPosts : {},
      likedPosts: user.likedPosts ? user.likedPosts : {},
    }));
    return usersArray;
  } catch (e) {
    console.error(`Fetching all users error: ${e}`);
  }
};

export const blockUser = async (userId) => {
  try {
    await update(ref(db, `users/${userId}`), {
      isBlocked: true,
    });
    console.log(`user ${userId} is blocked.`);
  } catch (e) {
    console.error(`Error while blocking the user:  ${e}`);
  }
};

export const unblockUser = async (userId) => {
  try {
    await update(ref(db, `users/${userId}`), {
      isBlocked: false,
    });
    console.log(`user ${userId} is unblocked.`);
  } catch (e) {
    console.error(`Error while unblocking the user:  ${e}`);
  }
};

export const deleteUser = async (userId) => {
  try {
    await remove(ref(db, `users/${userId}`));
    console.log(`User ${userId} is removed.`);
  } catch (e) {
    console.error(`Error while deleting user: ${e}`);
  }
};

export const uploadUserPhoto = async (fileBytes, currentUser, setLoading) => {
  const fileRef = storageRef(storage, `users/${currentUser.uid}`);
  const userRef = ref(db, `users/${currentUser.uid}`);
  try {
    setLoading(true);

    const snapshot = await uploadBytes(fileRef, fileBytes);
    const photoUrl = await getDownloadURL(fileRef);

    await update(userRef, {
      image: photoUrl,
    });

    updateProfile(currentUser, { photoURL: photoUrl });

    setLoading(false);
    console.error(`Photo has been uploaded!`);
    alert(`Image: ${fileBytes} has been been successfully uploaded`);
  } catch (e) {
    alert(`Error while uploading photo!`);
    console.error(`Error while uploading photo: ${e}!`);
  }
};
