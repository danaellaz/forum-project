import { set, ref, get, push, remove } from "firebase/database";
import { db } from "../config/firebase-config";

export const addLike = async (postId, likes) => {
  const likesRef = ref(db, `/posts/${postId}/likes`);
  await push(likesRef, likes);
};

export const getAllLikes = async (postId) => {
  const likesRef = ref(db, `/posts/${postId}/likes`);
  const snapshot = await get(likesRef);
  const likesData = snapshot.val();

  return likesData || [];
};

export const removeLike = async (postId, likeToRemove) => {
  const likesRef = ref(db, `/posts/${postId}/likes`);
  const snapshot = await get(likesRef);
  const currentLikes = snapshot.val();

  const keyToRemove = Object.keys(currentLikes).find((key) => {
    return currentLikes[key].author === likeToRemove.author;
  });

  if (keyToRemove) {
    delete currentLikes[keyToRemove];
    await set(likesRef, currentLikes);
  }
};
