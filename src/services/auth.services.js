import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from "@firebase/auth";
import { auth } from "../config/firebase-config";

export const registerUser = (email, password) => {
    return createUserWithEmailAndPassword(auth, email, password);
}

export const logInUser = (email, password) => {
    return signInWithEmailAndPassword(auth, email, password);
}

export const logOutUser = () => {
    return signOut(auth);
}