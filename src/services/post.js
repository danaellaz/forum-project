import {
  getDatabase,
  set,
  onValue,
  off,
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
} from "firebase/database";
import { db } from "../config/firebase-config";
import { getAllLikes } from "./likes";

export const createPost = (title, content, category) => {
  const newPostRef = push(ref(db, "posts"));
  return set(newPostRef, {
    title: title,
    content: content,
    category: category,
  });
};

export const getPosts = (callback) => {
  const postsRef = ref(db, "posts");
  onValue(postsRef, (snapshot) => {
    const postsData = snapshot.val();
    const postsArray = Object.entries(postsData || {}).map(([id, post]) => ({
      id,
      ...post,
      comments: post.comments ? post.comments : {},
    }));
    callback(postsArray);
  });
  return () => off(postsRef, "value");
};

export const deletePost = async (postId) => {
  await set(ref(db, `posts/${postId}`), null);
};

export const getAllPosts = async () => {
  const snapshot = await get(ref(db, "posts"));
  const postsData = snapshot.val();

  const postsArray = await Promise.all(
    Object.entries(postsData || {}).map(async ([id, post]) => {
      const likesData = await getAllLikes(id);
      return {
        id,
        ...post,
        likes: Object.keys(likesData).length,
        comments: post.comments ? post.comments : {},
      };
    })
  );

  return postsArray;
};

export const getImage = async (postId) => {
  const imageUrlRef = ref(db, `posts/${postId}/image`);

  return await get(imageUrlRef).then((imageUrlSnapshot) => {
    return imageUrlSnapshot.exists() ? imageUrlSnapshot.val() : null;
  });
};

const fromPostsDocument = (snapshot) => {
  const postsDocument = snapshot.val();

  return Object.keys(postsDocument).map((key) => {
    const post = postsDocument[key];

    return {
      ...post,
      id: key,
      createdOn: new Date(post.createdOn),
    };
  });
};

export const getPostsByAuthor = async (handle) => {
  return await get(
    query(ref(db, "posts"), orderByChild("author"), equalTo(handle))
  ).then((snapshot) => {
    if (!snapshot.exists()) return [];

    return fromPostsDocument(snapshot);
  });
};

export const getPostsByCategory = async (category) => {
  return await get(
    query(ref(db, "posts"), orderByChild("category"), equalTo(category))
  ).then((snapshot) => {
    if (!snapshot.exists()) return [];
    return fromPostsDocument(snapshot);
  });
};

export const fetchPostDetails = (id, setPost) => {
  const postRef = ref(getDatabase(), `posts/${id}`);

  const handlePostSnapshot = (snapshot) => {
    const postData = snapshot.val();
    if (postData) {
      const postWithId = { id, ...postData };
      setPost(postWithId);
    }
  };

  onValue(postRef, handlePostSnapshot);

  return () => {
    off(postRef, "value");
  };
};
