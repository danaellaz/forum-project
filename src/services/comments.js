import { ref, push, off, get, onValue, set, remove } from "firebase/database";
import { db } from "../config/firebase-config";

export const addComment = async (postId, comment) => {
  const commentsRef = ref(db, `/posts/${postId}/comments`);
  await push(commentsRef, comment);
};

export const getAllComments = (postId, callback) => {
  const commentsRef = ref(db, `/posts/${postId}/comments`);
  onValue(commentsRef, (snapshot) => {
    const commentsData = snapshot.val();
    callback(commentsData);
  });
  return () => off(commentsRef, "value");
};

export const deleteComment = async (postId, commentId) => {
  await remove(ref(db, `posts/${postId}/comments/${commentId}`));
}; 