import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Card, Row, Col } from "react-bootstrap";
import { fetchPostDetails } from "../services/post";
import { styled } from "styled-components";
import Comments from "../components/Comments/Comments";
import { useAuth } from "../provider/AuthProvider";
import { DeletePost } from "../components/DeletePost";
import { FaCrown } from "react-icons/fa";

const SinglePostView = () => {
  const [post, setPost] = useState({});
  const { id } = useParams();
  const { userData } = useAuth();

  useEffect(() => {
    const postData = fetchPostDetails(id, setPost);
    return postData;
  }, [id]);

  if (!post.id) {
    return <p>Post not found.</p>;
  }
  const srcImage = post.image
    ? post.image
    : "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/1665px-No-Image-Placeholder.svg.png";

  return (
    <div className="container mt-4">
      <div className="row justify-content-center">
        <div className="col-md-12">
          <Row className="mt-4">
            <Col md={{ span: 10, offset: 1 }}>
              <Card>
                <Card.Body>
                  <Card.Title>{post.title}</Card.Title>
                  <Card.Text>
                    <FaCrown /> {post.author}
                  </Card.Text>
                  <Card.Text>Category: {post.category}</Card.Text>
                  <Row>
                    <Col md={8}>
                      <Card>
                        <Card.Text>{post.content}</Card.Text>
                      </Card>
                    </Col>
                    <Col>
                      <PostImage src={srcImage} alt="Image of nature" />
                    </Col>
                    {userData?.username === post.author ? (
                      <Col>
                        <DeletePost post={post} />
                      </Col>
                    ) : (
                      <>{null}</>
                    )}
                    <Col>
                      <Comments />
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default SinglePostView;

const PostImage = styled.img`
  width: 200px;
  height: 200px;
`;

