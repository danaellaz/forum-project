import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import { AuthProvider } from "./provider/AuthProvider";
import Main from "./components/Main";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Login from "./components/Authentication/LogIn";
import Logout from "./components/Authentication/Logout";
import Signup from "./components/Authentication/Signup";
import CreatePost from "./components/Posts/CreatePost";
import SinglePostView from "./views/SinglePostView";
import ForgotPassword from "./components/Authentication/ForgotPassword";
import PrivateRoute from "./components/Authentication/PrivateRoute";
import { SearchResults } from "./components/Search/SearchResults";
import { UserProfile } from "./components/User/UserProfile";
import AdminDashboard from "./components/Admin/AdminDashboard";
import AdminRoute from "./components/Admin/adminRoute";
import { styled } from "styled-components";
import Categories from "./components/Categories";
import AboutUs from "./components/AboutUs";
import map3 from "./assets/map3.jpeg";

function App() {
  return (
    <Router>
      <AuthProvider>
        <Header />
        <Routes>
          <Route exact path="/" element={<Main />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/search/:term" element={<SearchResults />} />
          <Route element={<PrivateRoute />}>
            <Route path="/posts/create" element={<CreatePost />} />
            <Route
              path="/posts/view/:id"
              element={
                <SinglePostImage>
                  {" "}
                  <SinglePostView />{" "}
                </SinglePostImage>
              }
            />
            <Route path="user-info" element={<UserProfile />} />
            <Route path="/topics" element={<Categories />} />
            <Route path="/about-us" element={<AboutUs />} />
            <Route element={<AdminRoute />}>
              <Route path="/admin-dashboard" element={<AdminDashboard />} />
            </Route>
          </Route>
        </Routes>
        <Footer />
      </AuthProvider>
    </Router>
  );
}

export default App;

const SinglePostImage = styled.div`
  background-image: url(${map3});
  background-size: cover;
  min-height: 100vh;
  width: 100%;
`;
