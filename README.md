## Tourist Talks 
Welcome to Tourist Talks, forum where users can share their travel experience. Users can write, like and comment a post, edit their profile information, search and filter the posts. The administrators of the app can search, block and delete the users.

## Getting started
Follow the steps below to run Tourist Talks:

1. Clone this repository: `git clone https://gitlab.com/danaellaz/forum-project.git`
2. Navigate to the project directory: `cd forum-project`
3. Install dependencies: `npm install`
4. Start the development server: `npm run dev`